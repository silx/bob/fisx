# fisx

[![pipeline status](https://gitlab.esrf.fr/silx/bob/fisx/badges/master/pipeline.svg)](https://gitlab.esrf.fr/silx/bob/fisx/pipelines)

Build binary packages for [fisx](https://github.com/vasole/fisx): [Artifacts](https://silx.gitlab-pages.esrf.fr/bob/fisx)
